# Github Profile
Type in the Username you want to find more information and it will return the results of all github user whose username contains search keywords. You can see the picture, name, website, Bio, email, profile creation date, follower, following and total repos of that user. You can select the total repos to see the repos details.

# Stack
- ReactNative using create-react-native-app cli
- Github GraphQL API v4 (apollo-client & react-apollo)
- React navigation for routing

# Programming Language and Platform
JavaScript, React Native, Expo, Apollo, GraphQL

# How to use
- insert you access token into your config.js file
- yarn to install all npm packages
- yarn start to run


