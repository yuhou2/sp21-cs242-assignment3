import gql from 'graphql-tag';

const query = {
  fetchUser: gql`
    query fetchUser($login: String!){
        user(login: $login) {
            login
            name
            bio
            email
            websiteUrl
            createdAt
            avatarUrl
            followers(first: 20) {
                totalCount
                nodes {
                    login
                    avatarUrl
                    name
                }
            }
            following(first: 20) {
                totalCount
                nodes {
                    login
                    avatarUrl
                    name
                }
            }
            repositories(first: 50, isFork: false, orderBy: {field: STARGAZERS, direction: DESC}) {
                totalCount
                nodes {
                    owner{
                        login
                    }
                    nameWithOwner
                    name
                    description
                }
            }
        }
    }`,
  fetchUsers: gql`
    query fetchUsers($login: String!){
        search(type: USER, query: $login, first: 20) {
            edges {
                node {
                    ... on User{
                        login
                        name
                        bio
                        email
                        websiteUrl
                        createdAt
                        avatarUrl
                        followers(first: 20) {
                            totalCount
                            nodes {
                                login
                                avatarUrl
                                name
                            }
                        }
                        following(first: 20) {
                            totalCount
                            nodes {
                                login
                                avatarUrl
                                name
                            }
                        }
                        repositories(first: 50, isFork: false, orderBy: {field: STARGAZERS, direction: DESC}) {
                            totalCount
                            nodes {
                                owner{
                                    login
                                }
                                nameWithOwner
                                name
                                description
                            }
                        }
                    }
                }
            }
        }
    }`,
};

export default query;
