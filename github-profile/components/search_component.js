/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prop-types */
import React from 'react';
import { Text, FlatList, StyleSheet } from 'react-native';
import { graphql } from 'react-apollo';
import { Card } from 'react-native-paper';
import { withNavigation } from 'react-navigation';
import query from '../graphql_query';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    padding: 10,
  },
  card: {
    margin: 15,
    borderRadius: 15,
  },
});

export const SearchComponent = ({
  data: { loading, error, search },
  navigation: { navigate },
}) => {
  if (loading) return <Text>fetching users... </Text>;
  if (error) {
    return (
      <Text>
        {error.message}
      </Text>
    );
  }

  const userList = search.edges; // extracted after response loads

  return (
    <FlatList
      data={userList}
      keyExtractor={(item) => item.node.login.toString()}
      renderItem={({ item }) => (
        <Card style={styles.card} onPress={() => navigate('User', { username: item.node.login })}>
          <Card.Cover source={{ uri: item.node.avatarUrl }} />
          <Card.Title title={item.node.login} />
          <Card.Content>
            <Text>
              Name:
              {item.node.name}
            </Text>
            <Text>
              Bio:
              {item.node.bio}
            </Text>
            <Text>
              Email:
              {item.node.email}
            </Text>
            <Text>
              Website:
              {item.node.websiteUrl}
            </Text>
            <Text>
              Creation date:
              {item.node.createdAt}
            </Text>
            <Text>
              followers:
              {item.node.followers.totalCount}
            </Text>
            <Text>
              following:
              {item.node.following.totalCount}
            </Text>
            <Text>
              Total Repos:
              {item.node.repositories.totalCount}
            </Text>
          </Card.Content>
        </Card>
      )}
    />
  );
};

const Search = graphql(
  query.fetchUsers,
  {
    options: ({ searchQuery }) => ({ variables: { login: searchQuery } }),
  },
)(SearchComponent);

export default withNavigation(Search);
