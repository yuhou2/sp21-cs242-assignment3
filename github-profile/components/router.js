/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import Search from './search_component';
import Repo from './repo_profile';
import Home, { SEARCH_QUERY } from './home';
import Following from './following_profile';
import Follower from './follower_profile';
import User from './user_profile';

const NavStack = createStackNavigator({
  Home: { screen: Home, navigationOptions: { title: 'Home' } },
  Search: { screen: (props) => <Search {...props} searchQuery={SEARCH_QUERY} />, navigationOptions: { title: 'Search Results' } },
  Repo: { screen: Repo, navigationOptions: { title: 'Repositories Details' } },
  Follower: { screen: Follower, navigationOptions: { title: 'Follower Profiles' } },
  Following: { screen: Following, navigationOptions: { title: 'Following Profiles' } },
  User: { screen: User, navigationOptions: { title: 'User Profiles' } },
});

const Router = createAppContainer(NavStack);

export default Router;
