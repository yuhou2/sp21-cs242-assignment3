/* eslint-disable react/jsx-filename-extension */
/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
/* eslint-disable import/no-mutable-exports */
import * as React from 'react';
import {
  TextInput, Button, DefaultTheme, Provider as PaperProvider,
} from 'react-native-paper';

export let SEARCH_QUERY = '';

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#3498db',
    accent: '#f1c40f',
  },
};

const Home = ({ navigation: { navigate } }) => {
  const [text, setText] = React.useState('');

  SEARCH_QUERY = text;

  return (
    <PaperProvider theme={theme}>
      <TextInput
        label="Search Github"
        value={text}
        onChangeText={(text) => setText(text)}
        theme={theme}
        textAlign="center"
        style={{ height: 60, justifyContent: 'center' }}
      />
      <Button raised onPress={() => navigate('Search')}>
        Search
      </Button>
    </PaperProvider>
  );
};

export default Home;
