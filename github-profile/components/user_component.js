/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prop-types */
import React from 'react';
import {
  View, Text, SafeAreaView, StyleSheet,
} from 'react-native';
import {
  Avatar,
  Title,
  Caption,
  TouchableRipple,
} from 'react-native-paper';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userInfoSection: {
    paddingHorizontal: 30,
    marginBottom: 25,
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 34,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  rowItemText: {
    color: '#777777',
    marginLeft: 25,
    marginRight: 15,
    fontWeight: '600',
    fontSize: 20,
    lineHeight: 40,
  },
  menuWrapper: {
    marginTop: 25,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 25,
    fontWeight: '600',
    fontSize: 20,
    lineHeight: 30,
  },
});

function UserComponent({
  data: { loading, error, user },
  navigation: { navigate },
}) {
  if (loading) return <Text>Fetching users... </Text>;
  if (error) {
    return (
      <Text>
        {error.message}
      </Text>
    );
  }

  const {
    login, name, bio, email, websiteUrl, createdAt, avatarUrl, repositories, followers, following,
  } = user; // extracted after response loads

  return (
    <SafeAreaView style={styles.container}>

      <View style={styles.userInfoSection}>
        <View style={{ flexDirection: 'row', marginTop: 60 }}>
          <Avatar.Image
            source={{
              uri: avatarUrl,
            }}
            size={150}
          />
          <View style={{ marginLeft: 20, marginRight: 15 }}>
            <Title style={[styles.title, {
              marginTop: 15,
              marginBottom: 5,
              marginRight: 15,
            }]}
            >
              {login}
            </Title>
            <Caption style={styles.caption}>
              {name}
            </Caption>
            <Caption style={styles.caption}>
              {createdAt}
            </Caption>
          </View>
        </View>
      </View>

      <View style={styles.userInfoSection}>
        <View style={styles.row}>
          <Icon name="bio" color="#777777" size={40} />
          <Text style={styles.rowItemText}>{bio}</Text>
        </View>
        <View style={styles.row}>
          <Icon name="search-web" color="#777777" size={40} />
          <Text style={styles.rowItemText}>{websiteUrl}</Text>
        </View>
        <View style={styles.row}>
          <Icon name="email" color="#777777" size={40} />
          <Text style={styles.rowItemText}>{email}</Text>
        </View>
      </View>

      <View style={styles.menuWrapper}>
        <TouchableRipple onPress={() => navigate('Follower', { username: login })}>
          <View style={styles.menuItem}>
            <Icon name="heart-outline" color="#FF6347" size={40} />
            <Text style={styles.menuItemText}>
              Followers:
              {followers.totalCount}
            </Text>
          </View>
        </TouchableRipple>
        <TouchableRipple onPress={() => navigate('Following', { username: login })}>
          <View style={styles.menuItem}>
            <Icon name="heart-outline" color="#FF6347" size={40} />
            <Text style={styles.menuItemText}>
              Followings:
              {following.totalCount}
            </Text>
          </View>
        </TouchableRipple>
        <TouchableRipple onPress={() => navigate('Repo', { username: login })}>
          <View style={styles.menuItem}>
            <Icon name="contain" color="#FF6347" size={40} />
            <Text style={styles.menuItemText}>
              Total Repos:
              {repositories.totalCount}
            </Text>
          </View>
        </TouchableRipple>
      </View>
    </SafeAreaView>
  );
}

export default withNavigation(UserComponent);
