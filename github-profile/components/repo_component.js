/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prop-types */
import React from 'react';
import { Text, FlatList, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Card } from 'react-native-paper';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    padding: 10,
  },
  card: {
    margin: 8,
    borderRadius: 15,
  },
});

const RepoComponent = ({ data: { loading, error, user }, navigation: { navigate } }) => {
  if (loading) return <Text>Fetching repos... </Text>;
  if (error) {
    return (
      <Text>
        {error.message}
      </Text>
    );
  }

  const responseData = user.repositories.nodes;

  return (
    <FlatList
      data={responseData}
      keyExtractor={(item) => item.name.toString()}
      renderItem={({ item }) => (
        <Card style={styles.card} onPress={() => navigate('User', { username: item.owner.login })}>
          <Card.Title title={item.name} />
          <Card.Content>
            <Text>
              Repository Name:
              {item.name}
            </Text>
            <Text>
              Owner Name:
              {item.owner.login}
            </Text>
            <Text>
              Description:
              {item.description}
            </Text>
          </Card.Content>
        </Card>
      )}
    />
  );
};

export default withNavigation(RepoComponent);
