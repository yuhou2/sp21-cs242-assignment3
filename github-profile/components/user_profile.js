/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prop-types */
import React from 'react';
import { graphql } from 'react-apollo';
import UserComponent from './user_component';
import query from '../graphql_query';

const User = ({
  navigation: {
    state: {
      params: { username },
    },
    navigate,
  },
}) => <UserDetail username={username} navigate={navigate} />;

export const UserDetail = graphql(
  query.fetchUser,
  {
    options: ({ username }) => ({ variables: { login: username } }),
  },
)(UserComponent);

export default User;
