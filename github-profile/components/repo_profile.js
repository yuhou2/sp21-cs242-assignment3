/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prop-types */
import React from 'react';
import { graphql } from 'react-apollo';
import RepoComponent from './repo_component';
import query from '../graphql_query';

const Repo = ({
  navigation: {
    state: {
      params: { username },
    },
    navigate,
  },
}) => <RepoDetail username={username} navigate={navigate} />;

export const RepoDetail = graphql(
  query.fetchUser,
  {
    options: ({ username }) => ({ variables: { login: username } }),
  },
)(RepoComponent);

export default Repo;
