/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { Text, FlatList, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Card } from 'react-native-paper';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    padding: 10,
  },
  card: {
    margin: 8,
    borderRadius: 15,
  },
});

const FollowerComponent = ({ data: { loading, error, user }, navigation: { navigate } }) => {
  if (loading) return <Text>Fetching followers... </Text>;
  if (error) {
    return (
      <Text>
        {error.message}
      </Text>
    );
  }

  const responseData = user.followers.nodes;

  return (
    <FlatList
      data={responseData}
      keyExtractor={(item) => item.login.toString()}
      renderItem={({ item }) => (
        <Card style={styles.card} testID="follower button" onPress={() => navigate('User', { username: item.login })}>
          <Card.Cover source={{ uri: item.avatarUrl }} />
          <Card.Title title={item.login} />
          <Card.Content>
            <Text>
              Name:
              {item.name}
            </Text>
            <Text>
              User Name:
              {item.login}
            </Text>
          </Card.Content>
        </Card>
      )}
    />
  );
};

export default withNavigation(FollowerComponent);
