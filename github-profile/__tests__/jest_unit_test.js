/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-boost';
import { ApolloLink } from 'apollo-link';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import renderer from 'react-test-renderer';
import fetch from 'node-fetch';
import { render, fireEvent } from 'react-native-testing-library';
import { GITHUB_ACCESS_TOKEN } from '../config';
import Home from '../components/home';
import { UserDetail } from '../components/user_profile';
import { RepoDetail } from '../components/repo_profile';
import Search, { SearchComponent } from '../components/search_component';
import { FollowerDetail } from '../components/follower_profile';
import { FollowingDetail } from '../components/following_profile';
import FollowingComponent from '../components/following_component';
import FollowerComponent from '../components/follower_component';
import RepoComponent from '../components/repo_component';
import UserComponent from '../components/user_component';

jest.useFakeTimers();
jest.mock('react-navigation', () => ({ withNavigation: (component) => component }));
jest.setTimeout(60000);

const navigation = { navigate: jest.fn() };

const navigationPara = {
  username: 'Gregg',
  navigation: { navigate: jest.fn() },
};

const navigationError = {
  data: { error: { message: 'NetworkError' } },
  navigation: { navigate: jest.fn() },
};

const navigationFollow = {
  data: {
    error: false,
    loading: false,
    user:
    {
      following: {
        totalCount: 1,
        nodes: [
          {
            __typename: 'User',
            avatarUrl: 'https://avatars.githubusercontent.com/u/1097722?v=4',
            login: 'kalineh',
            name: null,
          },
        ],
      },
      followers: {
        totalCount: 1,
        nodes: [
          {
            __typename: 'User',
            avatarUrl: 'https://avatars.githubusercontent.com/u/1097722?v=4',
            login: 'kalineh',
            name: null,
          },
        ],
      },
    },
  },
  navigation: { navigate: jest.fn() },
};

function createClient() {
  const httpLink = createHttpLink({
    uri: 'https://api.github.com/graphql',
    fetch,
  });

  // apply widdleware to add access token to request
  const middlewareLink = new ApolloLink((operation, forward) => {
    operation.setContext({
      headers: {
        authorization: `Bearer ${GITHUB_ACCESS_TOKEN}`,
      },
    });
    return forward(operation);
  });
  const link = middlewareLink.concat(httpLink);

  // Initialize Apollo Client with URL to our server
  return new ApolloClient({
    link,
    cache: new InMemoryCache(),
  });
}

it('search screen', () => {
  const component = renderer.create(
    <ApolloProvider client={createClient()}>
      <Search searchQuery="YunesHou" navigation={navigation} />
    </ApolloProvider>,
  );

  const rendered = component.toJSON();
  expect(rendered).toMatchSnapshot();
});

it('home screen', () => {
  const rendered = renderer.create(<Home navigation={navigation} />).toJSON();
  expect(rendered).toMatchSnapshot();
});

it('user screen', () => {
  const component = renderer.create(
    <ApolloProvider client={createClient()}>
      <UserDetail {...navigationPara} />
    </ApolloProvider>,
  );

  const rendered = component.toJSON();
  expect(rendered).toMatchSnapshot();
});

it('repo screen', () => {
  const component = renderer.create(
    <ApolloProvider client={createClient()}>
      <RepoDetail {...navigationPara} />
    </ApolloProvider>,
  );

  const rendered = component.toJSON();
  expect(rendered).toMatchSnapshot();
});

it('follow screen', () => {
  const component = renderer.create(
    <ApolloProvider client={createClient()}>
      <FollowingDetail {...navigationPara} />
    </ApolloProvider>,
  );

  const rendered = component.toJSON();
  expect(rendered).toMatchSnapshot();
});

it('following screen', () => {
  const component = renderer.create(
    <ApolloProvider client={createClient()}>
      <FollowerDetail {...navigationPara} />
    </ApolloProvider>,
  );

  const rendered = component.toJSON();
  expect(rendered).toMatchSnapshot();
});

it('search loading', () => {
  const component = renderer
    .create(
      <ApolloProvider client={createClient()}>
        <Search searchQuery="YunesHou" navigation={navigation} />
      </ApolloProvider>,
    )
    .toJSON();
  expect(component.children).toStrictEqual(['fetching users... ']);
});

it('user loading', () => {
  const component = renderer
    .create(
      <ApolloProvider client={createClient()}>
        <UserDetail {...navigationPara} />
      </ApolloProvider>,
    )
    .toJSON();
  expect(component.children).toStrictEqual(['Fetching users... ']);
});

it('repo loading', () => {
  const component = renderer
    .create(
      <ApolloProvider client={createClient()}>
        <RepoDetail {...navigationPara} />
      </ApolloProvider>,
    )
    .toJSON();
  expect(component.children).toStrictEqual(['Fetching repos... ']);
});

it('follower loading', () => {
  const component = renderer
    .create(
      <ApolloProvider client={createClient()}>
        <FollowerDetail {...navigationPara} />
      </ApolloProvider>,
    )
    .toJSON();
  expect(component.children).toStrictEqual(['Fetching followers... ']);
});

it('following loading', () => {
  const component = renderer
    .create(
      <ApolloProvider client={createClient()}>
        <FollowingDetail {...navigationPara} />
      </ApolloProvider>,
    )
    .toJSON();
  expect(component.children).toStrictEqual(['Fetching followings... ']);
});

it('following navigation', () => {
  const { getByTestId } = render(
    <ApolloProvider client={createClient()}>
      <FollowingComponent {...navigationFollow} />
    </ApolloProvider>,
  );
  fireEvent.press(getByTestId('following button'));
  expect(navigationFollow.navigation.navigate).toHaveBeenCalled();
});

it('follower navigation', () => {
  const { getByTestId } = render(
    <ApolloProvider client={createClient()}>
      <FollowerComponent {...navigationFollow} />
    </ApolloProvider>,
  );
  fireEvent.press(getByTestId('follower button'));
  expect(navigationFollow.navigation.navigate).toHaveBeenCalled();
});

it('following error', () => {
  const component = renderer
    .create(
      <ApolloProvider client={createClient()}>
        <FollowingComponent {...navigationError} />
      </ApolloProvider>,
    )
    .toJSON();
  expect(component.children).toStrictEqual(['NetworkError']);
});

it('follower error', () => {
  const component = renderer
    .create(
      <ApolloProvider client={createClient()}>
        <FollowerComponent {...navigationError} />
      </ApolloProvider>,
    )
    .toJSON();
  expect(component.children).toStrictEqual(['NetworkError']);
});

it('user error', () => {
  const component = renderer
    .create(
      <ApolloProvider client={createClient()}>
        <UserComponent {...navigationError} />
      </ApolloProvider>,
    )
    .toJSON();
  expect(component.children).toStrictEqual(['NetworkError']);
});

it('repo error', () => {
  const component = renderer
    .create(
      <ApolloProvider client={createClient()}>
        <RepoComponent {...navigationError} />
      </ApolloProvider>,
    )
    .toJSON();
  expect(component.children).toStrictEqual(['NetworkError']);
});

it('search error', () => {
  const component = renderer
    .create(
      <ApolloProvider client={createClient()}>
        <SearchComponent {...navigationError} />
      </ApolloProvider>,
    )
    .toJSON();
  expect(component.children).toStrictEqual(['NetworkError']);
});
